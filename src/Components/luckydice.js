import { Component } from "react";
import dice from "../assets/images/dice.png";
import dice1 from "../assets/images/1.png";
import dice2 from "../assets/images/2.png";
import dice3 from "../assets/images/3.png";
import dice4 from "../assets/images/4.png";
import dice5 from "../assets/images/5.png";
import dice6 from "../assets/images/6.png";
// import renderImage from "./renderImage";
class RenderImage extends Component{
    constructor(props){
        super(props)
        console.log(this.props, 'đã vào')
    }
    render(){
        return (<>
            <img src={this.props.value && this.props.value.length > 0 ? this.props.value[0].src : dice1} width = "200px" height={"200px"} alt = "dice1"/>
        </>)
    }
}
// {/* <img src={this.state.value && this.state.value.length > 0 ? this.state.value[0].src : dice1} width = "200px" height={"200px"} alt = "dice1"/> */}
class LuckyDice extends Component{
    constructor(props){
        super(props)
        this.state = {
            xucxac : [
                {dice : 1 , src : dice1},
                {dice : 2 , src : dice2},
                {dice : 3 , src : dice3},
                {dice : 4 , src : dice4},
                {dice : 5 , src : dice5},
                {dice : 6 , src : dice6},
            ],
            dice: 1,
            value: []
        }
        this.onBtnLetDice = this.onBtnLetDice.bind(this);  
    }
    onBtnLetDice(){
        this.state.dice = Math.floor(Math.random() * 6) + 1;
        this.setState({
            value :  this.state.xucxac.filter(item => item.dice === this.state.dice)
        });
    }
    render(){
            return(
                <>
                    <div className="text-center">
                        <div className="row">
                            <div className="col-sm-12">
                                <h4>Lucky Dice Try Your Luck !</h4>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-sm-12">
                                <button onClick={this.onBtnLetDice} className="btn btn-primary">Lets Dice !</button>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-sm-12" >
                                {/* <RenderImag value={this.state && this.state.value && this.state.value.src ? this.state.value.src : dice1}></RenderImag> */}
                                <RenderImage value={this.state.value}></RenderImage>
                            </div>
                        </div>
                    </div>
                </>
            )
        }
}
export default LuckyDice


// export default function test(props){

// }